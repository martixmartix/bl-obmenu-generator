## Installation of bl-obmenu-generator

* Detailed installation instructions can be found [here](https://forums.bunsenlabs.org/viewtopic.php?pid=54559#p54559).

* Required dependencies:

    - perl>=5.14.0
    - perl-data-dump ([Data::Dump](https://metacpan.org/pod/Data::Dump))
    - perl-linux-desktopfiles>=0.09 ([Linux::DesktopFiles](https://metacpan.org/pod/Linux::DesktopFiles))

### Installation process:

- place the `bl-obmenu-generator` file inside your PATH
- place the `schema.pl` file inside `~/.config/bl-obmenu-generator/`

### Running:

- to generate a dynamic menu with icons, execute: `bl-obmenu-generator -i -p`
- to generate a static menu without icons, execute: `bl-obmenu-generator -s -c`
- to update the icons after changing icon theme, execute: `bl-obmenu-generator -i -d`

### Customizing:

- the `schema.pl` file provides a direct customization access for the menu that will be generated
- adding or removing a given entry from the `schema.pl` file is reflected inside the generated menu



THIS PROJECT MOVED TO https://notabug.org/martix/bl-obmenu-generator


bl-obmenu-generator
===================

Openbox menu generator with support for icons tailored according
to the specifics of the GNU/Linux distribution BunsenLabs.

---

```
usage: bl-obmenu-generator [options]

menu:
    -p         : generate a dynamic menu (pipe)
    -s         : generate a static menu
    -i         : include icons
    -m <id>    : menu id (default: 'root-menu')
    -t <label> : menu label text (default: 'Applications')

other:
    -S <file>  : path to the schema.pl file
    -C <file>  : path to the config.pl file
    -o <file>  : path to the menu.xml file
    -u         : update the config file
    -r         : regenerate the config file
    -d         : regenerate icons.db
    -c         : reconfigure openbox automatically
    -R         : reconfigure openbox and exit
    -h         : print this message and exit
    -v         : print version and exit

examples:
        bl-obmenu-generator -p -i     # dynamic menu with icons
        bl-obmenu-generator -s -c     # static menu without icons
        bl-obmenu-generator -i -d     # regenerate icons.db (required after changing icon theme)

* <b>Config file:</b> `~/.config/bl-obmenu-generator/config.pl`
* <b>Schema file:</b> `~/.config/bl-obmenu-generator/schema.pl`

---

Installation: [INSTALL.md](INSTALL.md)

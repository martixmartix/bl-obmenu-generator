#!/usr/bin/perl

# bl-obmenu-generator - schema file

=for comment

    item:      add an item inside the menu               {item => ["command", "label", "icon"]},
    cat:       add a category inside the menu             {cat => ["name", "label", "icon"]},
    sep:       horizontal line separator                  {sep => undef}, {sep => "label"},
    pipe:      a pipe menu entry                         {pipe => ["command", "label", "icon"]},
    raw:       any valid Openbox XML string               {raw => q(xml string)},
    begin_cat: begin of a category                  {begin_cat => ["name", "icon"]},
    end_cat:   end of a category                      {end_cat => undef},
    obgenmenu: generic menu settings                {obgenmenu => ["label", "icon"]},
    exit:      default "Exit" action                     {exit => ["label", "icon"]},

=cut

# NOTE:
#    * Keys and values are case sensitive. Keep all keys lowercase.
#    * ICON can be a either a direct path to an icon or a valid icon name
#    * Category names are case insensitive. (X-XFCE and x_xfce are equivalent)

require "$ENV{HOME}/.config/bl-obmenu-generator/config.pl";

# Text editor
my $editor = $CONFIG->{editor};



## Menu Structure
our $SCHEMA = [

#   	  Item		Command		Label		Icon 
        {item => ['gmrun', 'Run Program', 'system-run']},

	
	### Separator ---------------------------------------------------------       
	{sep => undef},
 
#   	   Item			Command			Label			Icon        
        {item => ['x-terminal-emulator', 'Terminal', 'terminal']},
        {item => ['x-www-browser', 'Web Browser', 'web-browser']},
        {item => ['bl-file-manager', 'File Manager', 'file-manager']},
        {item => ['bl-text-editor', 'Text Editor', 'text-editor']},
        {item => ['bl-media-player', 'Media Player', 'multimedia-player']},

	
	### Separator ---------------------------------------------------------        
	{sep => undef},
    
    # Menu categories
        
#	 Category   Name			Label				Icon 
    {cat => ['utility',     'Accessories', 'applications-utilities']},
  # {cat => ['development', 'Development', 'applications-development']},
  # {cat => ['education',   'Education',   'applications-science']},
    {cat => ['game',        'Games',       'applications-games']},
    {cat => ['graphics',    'Graphics',    'applications-graphics']},
    {cat => ['audiovideo',  'Multimedia',  'applications-multimedia']},
    {cat => ['network',     'Network',     'applications-internet']},
    {cat => ['office',      'Office',      'applications-office']},
  # {cat => ['other',       'Other',       'applications-other']},
    {cat => ['settings',    'Settings',    'applications-accessories']},
    {cat => ['system',      'System Tools','applications-system']},
  # {cat => ['qt',          'QT Applications',    'qtcreator']},
  # {cat => ['gtk',         'GTK Applications',   'gtk-properties']},
  # {cat => ['x_xfce',      'XFCE Applications',  'applications-other']},
  # {cat => ['gnome',       'GNOME Applications', 'gnome-applications']},
  # {cat => ['consoleonly', 'CLI Applications',   'applications-utilities']},

# How to add a custom category:

    #                  LABEL          ICON
    #{begin_cat => ['My category',  'cat-icon']},
    #             ... some items ...    {item => ["command", "label", "icon"]},
    #{end_cat   => undef},

# LXDE Menu entry
	#{pipe => ['openbox-menu lxde-applications.menu', 'LXDE', 'lxde_badge-symbolic']},
	
	
	### Separator ---------------------------------------------------------
	{sep => undef},
    
    ## Pipe menu categories for #BĹ Places and Recent Files
    
    {pipe => ['bl-places-pipemenu', 'Places', 'gtk-directory']},
    {pipe => ['bl-recent-files-pipemenu', 'Recent Files', 'document-open-recent']},
    
    
    ### Separator ---------------------------------------------------------
   # {sep       => undef},

# Generic menu settings    
   # {obgenmenu => ['Generic menu settings', 'configuration_section',]},
    
    
    ### Separator ---------------------------------------------------------
    {sep       => undef},
 
    {begin_cat => ["Preferences", "preferences-other"]},
		
		{begin_cat => ["Openbox", "preferences-system-windows"]},
						
			{item => ['bl-text-editor ~/.config/openbox/menu.xml', 'Edit menu.xml', '/usr/share/icons/Faenza/apps/22/menu-editor.png']},
							
		  # {item => ['bl-text-editor ~/.config/obmenu-generator/schema.pl', 'Edit schema.pl', 'menu-editor']},
					
			{item => ['bl-text-editor ~/.config/openbox/rc.xml', 'Edit rc.xml', 'text-editor']},
			
		  #	{item => ['martix-bl-switch-menu', 'Switch Menu', '/usr/share/icons/Faenza/apps/22/system-switch-user.png']},
					
			{item => ['bl-text-editor ~/.config/openbox/autostart', 'Edit autostart', '/usr/share/icons/Faenza/apps/22/startupmanager.png']},  
							
			{item => ['obmenu', 'GUI Menu Editor', '/usr/share/icons/Faenza/apps/22/awn-settings.png']},
							
			{item => ['obconf', 'GUI Config Tool', 'gtk-preferences']},
							
			{item => ['yad --button="OK":0 --center --window-icon=distributor-logo-bunsenlabs --text-info --title="How to Edit the Menu" --filename=/usr/share/bunsen/docs/helpfile-menu.txt --width=900 --height=700 --fontname=Monospace', 'How to Edit Menu', 'dialog-information']},
							
			{item => ['openbox --reconfigure', 'Reconfigure', '/usr/share/icons/Faenza/apps/22/gnome-disks.png']},
							
			{item => ['openbox --restart', 'Restart', '/usr/share/icons/Faenza/actions/22/system-restart-panel.png']},
					
		{end_cat => undef},
    
			{pipe => ['bl-compositor', 'Compositor', 'window-new']},			
					
			{pipe => ['bl-conky-pipemenu', 'Conky', 'utilities-system-monitor']},
					
			{pipe => ['bl-tint2-pipemenu', 'Tint2', 'taskbar']},
    
		{item => ['lxappearance', 'Appearance', '/usr/share/icons/Faenza/apps/22/xfce-ui.png']},
		
		{item => ['nitrogen', 'Choose Wallpaper', 'nitrogen']},
		
		{item => ['xfce4-notifyd-config', 'Notifications', 'xfce4-notifyd']},
		
		{item => ['xfce4-power-manager-settings', 'Power Management', 'battery-full']},
		
		
		{begin_cat => ["dmenu", "/usr/share/icons/Faenza/apps/22/menu-editor.png"]},
    
			{item => ['bl-text-editor ~/.config/dmenu/dmenu-bind.sh', 'Edit Start-Up Script', '/usr/share/icons/Faenza/apps/22/menu-editor.png']},
						
			{item => ['x-terminal-emulator -T "dmenu man page" -e man dmenu', 'Help?', 'stock_dialog-question']},
    
		{end_cat => undef},
		
		
		{begin_cat => ["gmrun", "system-run"]},
    
			{item => ['bl-text-editor ~/.gmrunrc', 'Edit Config File', '/usr/share/icons/Faenza/apps/22/menu-editor.png']},
						
			{item => ['x-terminal-emulator -T "gmrun man page" -e man gmrun', 'Help?', 'stock_dialog-question']},
    
		{end_cat => undef},
					
	
		{begin_cat => ["Display", "display"]},
    
			{item => ['arandr', 'Display Settings', '/usr/share/icons/Faenza/apps/22/system-config-displayca.png']},
						
			{item => ['x-terminal-emulator -T "xrandr man page" -e man xrandr', 'Help?', 'stock_dialog-question']},
						
		{end_cat => undef},
		
    {end_cat => undef},
  
    {begin_cat => ["System", "system"]},
    
		{pipe => ['bl-printing-pipemenu', 'Printers', 'printer']},
			
		{item => ['gksudo synaptic', 'Synaptic Package Manager', 'synaptic']},
				
		{item => ['gksudo bl-file-manager', 'File Manager as Root', 'file-manager']},
				
		{item => ['gksudo bl-text-editor', 'Text Editor as Root', 'text-editor']},
				
		{item => ['gksudo bl-text-editor /etc/lightdm/lightdm-gtk-greeter.conf /etc/lightdm/lightdm.conf', 'Login Settings', 'avatar-default']},
				
		{item => ['gksudo gparted', 'GParted', 'gparted']},
				
		{item => ['gksudo galternatives', 'Edit Debian Alternatives', 'galternatives']},
				
		{item => ['yad --button="OK":0 --center --window-icon=distributor-logo-bunsenlabs --text-info --title="About Bunsen Alternatives" --filename="/usr/share/bunsen/docs/helpfile-bl-alternatives.txt" --width=900 --height=700 --fontname=Monospace', 'About Bunsen Alternatives', 'distributor-logo-bunsenlabs']},
    
    {end_cat => undef},
    
    {pipe => ['bl-help-pipemenu', 'Help', 'help']},
   
	
	### Separator ---------------------------------------------------------
    {sep => undef},
	
	{pipe => ['bl-kb-pipemenu', 'Display Keybinds', 'preferences-desktop-keyboard-shortcuts']},
 	
 	
 	### Separator ---------------------------------------------------------
 	{sep => undef},
	
	{item => ['bl-lock', 'Lock Screen', 'system-lock-screen']},
	
	{item => ['bl-exit', 'Exit', 'xfce-system-exit']},
]
